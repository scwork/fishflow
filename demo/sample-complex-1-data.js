/**
 * 只有节点链表信息，不包含任何节点的位置信息，节点位置由其它程序进行自动布局处理
 * 由于数据量比较大，手写JSON实现节点布局是不现实的。因此采取节点对象方式。
 */
var demoLinkedNodes = {
    "name": "A0_START",
    "describe": "开始",
    "nodeType": "node",
    "unitNodeType": 0,
    "maxDepth": 8,
    "maxBreadth": 11,
    "last": false,
    "nextUnits": [
        {
            "name": "N-1",
            "describe": "单元-1",
            "nodeType": "node",
            "unitNodeType": 1,
            "maxDepth": 7,
            "maxBreadth": 7,
            "last": false,
            "nextUnits": [
                {
                    "name": "N-1-1",
                    "describe": "单元-1-1",
                    "nodeType": "node",
                    "unitNodeType": 1,
                    "maxDepth": 6,
                    "maxBreadth": 2,
                    "last": false,
                    "nextUnits": [
                        {
                            "name": "N-1-1-1",
                            "describe": "单元-1-1-1",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 5,
                            "maxBreadth": 1,
                            "last": false,
                            "nextUnits": [
                                {
                                    "name": "N-1-1-1-1",
                                    "describe": "单元-1-1-1-1",
                                    "nodeType": "node",
                                    "unitNodeType": 1,
                                    "maxDepth": 4,
                                    "maxBreadth": 1,
                                    "last": false,
                                    "nextUnits": [
                                        {
                                            "name": "N-1-1-1-1-1",
                                            "describe": "单元-1-1-1-1-1",
                                            "nodeType": "node",
                                            "unitNodeType": 1,
                                            "maxDepth": 3,
                                            "maxBreadth": 1,
                                            "last": false,
                                            "nextUnits": [
                                                {
                                                    "name": "SO-1",
                                                    "describe": "收-1",
                                                    "nodeType": "node",
                                                    "unitNodeType": 1,
                                                    "maxDepth": 2,
                                                    "maxBreadth": 1,
                                                    "last": true,
                                                    "nextUnits": [
                                                        {
                                                            "name": "Z9_END",
                                                            "describe": "结束",
                                                            "nodeType": "node",
                                                            "unitNodeType": 9,
                                                            "maxDepth": 1,
                                                            "maxBreadth": 1,
                                                            "last": false,
                                                            "nextUnits": []
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "name": "N-1-1-2",
                            "describe": "单元-1-1-2",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 3,
                            "maxBreadth": 1,
                            "last": false,
                            "nextUnits": [
                                {
                                    "name": "SO-1",
                                    "describe": "收-1",
                                    "nodeType": "node",
                                    "unitNodeType": 1,
                                    "maxDepth": 2,
                                    "maxBreadth": 1,
                                    "last": true,
                                    "nextUnits": [
                                        {
                                            "name": "Z9_END",
                                            "describe": "结束",
                                            "nodeType": "node",
                                            "unitNodeType": 9,
                                            "maxDepth": 1,
                                            "maxBreadth": 1,
                                            "last": false,
                                            "nextUnits": []
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "name": "N-1-2",
                    "describe": "单元-1-2",
                    "nodeType": "node",
                    "unitNodeType": 1,
                    "maxDepth": 4,
                    "maxBreadth": 2,
                    "last": false,
                    "nextUnits": [
                        {
                            "name": "N-1-2-1",
                            "describe": "单元-1-2-1",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 3,
                            "maxBreadth": 1,
                            "last": false,
                            "nextUnits": [
                                {
                                    "name": "N-1-2-1-1",
                                    "describe": "单元-1-2-1-1",
                                    "nodeType": "node",
                                    "unitNodeType": 1,
                                    "maxDepth": 2,
                                    "maxBreadth": 1,
                                    "last": true,
                                    "nextUnits": [
                                        {
                                            "name": "Z9_END",
                                            "describe": "结束",
                                            "nodeType": "node",
                                            "unitNodeType": 9,
                                            "maxDepth": 1,
                                            "maxBreadth": 1,
                                            "last": false,
                                            "nextUnits": []
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "name": "N-1-2-2",
                            "describe": "单元-1-2-2",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 2,
                            "maxBreadth": 1,
                            "last": true,
                            "nextUnits": [
                                {
                                    "name": "Z9_END",
                                    "describe": "结束",
                                    "nodeType": "node",
                                    "unitNodeType": 9,
                                    "maxDepth": 1,
                                    "maxBreadth": 1,
                                    "last": false,
                                    "nextUnits": []
                                }
                            ]
                        }
                    ]
                },
                {
                    "name": "N-1-3",
                    "describe": "单元-1-3",
                    "nodeType": "node",
                    "unitNodeType": 1,
                    "maxDepth": 3,
                    "maxBreadth": 3,
                    "last": false,
                    "nextUnits": [
                        {
                            "name": "N-1-3-1",
                            "describe": "单元-1-3-1",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 2,
                            "maxBreadth": 1,
                            "last": true,
                            "nextUnits": [
                                {
                                    "name": "Z9_END",
                                    "describe": "结束",
                                    "nodeType": "node",
                                    "unitNodeType": 9,
                                    "maxDepth": 1,
                                    "maxBreadth": 1,
                                    "last": false,
                                    "nextUnits": []
                                }
                            ]
                        },
                        {
                            "name": "N-1-3-2",
                            "describe": "单元-1-3-2",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 2,
                            "maxBreadth": 1,
                            "last": true,
                            "nextUnits": [
                                {
                                    "name": "Z9_END",
                                    "describe": "结束",
                                    "nodeType": "node",
                                    "unitNodeType": 9,
                                    "maxDepth": 1,
                                    "maxBreadth": 1,
                                    "last": false,
                                    "nextUnits": []
                                }
                            ]
                        },
                        {
                            "name": "N-1-3-3",
                            "describe": "单元-1-3-3",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 2,
                            "maxBreadth": 1,
                            "last": true,
                            "nextUnits": [
                                {
                                    "name": "Z9_END",
                                    "describe": "结束",
                                    "nodeType": "node",
                                    "unitNodeType": 9,
                                    "maxDepth": 1,
                                    "maxBreadth": 1,
                                    "last": false,
                                    "nextUnits": []
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "name": "N-2",
            "describe": "单元-2",
            "nodeType": "node",
            "unitNodeType": 1,
            "maxDepth": 5,
            "maxBreadth": 3,
            "last": false,
            "nextUnits": [
                {
                    "name": "N-2-1",
                    "describe": "单元-2-1",
                    "nodeType": "node",
                    "unitNodeType": 1,
                    "maxDepth": 4,
                    "maxBreadth": 3,
                    "last": false,
                    "nextUnits": [
                        {
                            "name": "N-2-1-1",
                            "describe": "单元-2-1-1",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 3,
                            "maxBreadth": 2,
                            "last": false,
                            "nextUnits": [
                                {
                                    "name": "N-2-1-1-1",
                                    "describe": "单元-2-1-1-1",
                                    "nodeType": "node",
                                    "unitNodeType": 1,
                                    "maxDepth": 2,
                                    "maxBreadth": 1,
                                    "last": true,
                                    "nextUnits": [
                                        {
                                            "name": "Z9_END",
                                            "describe": "结束",
                                            "nodeType": "node",
                                            "unitNodeType": 9,
                                            "maxDepth": 1,
                                            "maxBreadth": 1,
                                            "last": false,
                                            "nextUnits": []
                                        }
                                    ]
                                },
                                {
                                    "name": "N-2-1-1-2",
                                    "describe": "单元-2-1-1-2",
                                    "nodeType": "node",
                                    "unitNodeType": 1,
                                    "maxDepth": 2,
                                    "maxBreadth": 1,
                                    "last": true,
                                    "nextUnits": [
                                        {
                                            "name": "Z9_END",
                                            "describe": "结束",
                                            "nodeType": "node",
                                            "unitNodeType": 9,
                                            "maxDepth": 1,
                                            "maxBreadth": 1,
                                            "last": false,
                                            "nextUnits": []
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "name": "N-2-1-2",
                            "describe": "单元-2-1-2",
                            "nodeType": "node",
                            "unitNodeType": 1,
                            "maxDepth": 3,
                            "maxBreadth": 1,
                            "last": false,
                            "nextUnits": [
                                {
                                    "name": "N-2-1-2-1",
                                    "describe": "单元-2-1-2-1",
                                    "nodeType": "node",
                                    "unitNodeType": 1,
                                    "maxDepth": 2,
                                    "maxBreadth": 1,
                                    "last": true,
                                    "nextUnits": [
                                        {
                                            "name": "Z9_END",
                                            "describe": "结束",
                                            "nodeType": "node",
                                            "unitNodeType": 9,
                                            "maxDepth": 1,
                                            "maxBreadth": 1,
                                            "last": false,
                                            "nextUnits": []
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "name": "N-3",
            "describe": "单元-3",
            "nodeType": "node",
            "unitNodeType": 1,
            "maxDepth": 2,
            "maxBreadth": 1,
            "last": true,
            "nextUnits": [
                {
                    "name": "Z9_END",
                    "describe": "结束",
                    "nodeType": "node",
                    "unitNodeType": 9,
                    "maxDepth": 1,
                    "maxBreadth": 1,
                    "last": false,
                    "nextUnits": []
                }
            ]
        }
    ]
};
